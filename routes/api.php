<?php

use App\Http\Controllers\ApisCont;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::group(['middleware'=>'auth:sanctum'],function(){

    //Add Pizza Details
    Route::get('PizzaDetail',[ApisCont::class,'ShowPizzaDetail']);

    //Add Order
    Route::post('order',[ApisCont::class,'orders']);

    //Update Password
    Route::post('changePassword',[ApisCont::class,'changedPassword']);

});

// Login Customer
Route::post('login',[ApisCont::class,'login']);

//Register Customer
Route::post('register',[ApisCont::class,'register']);

//Add Banners
Route::get('Banner',[ApisCont::class,'GetImage']);

Route::get('Categories',[ApisCont::class,'ShowCategories']);

Route::get('Category/{id}',[ApisCont::class,'showCategory']);


Route::post('ForgetPassword',[ApisCont::class,'ForgetPassword']);


Route::get('CusOrderHistory/{id}',[ApisCont::class,'CustomerOrderHistory']);


Route::get('Category/{id}',[ApisCont::class,'showCategory']);

Route::get('coupenCode/{coupen_Codee}',[ApisCont::class,'coupenCodeData']);

Route::get('logo',[ApisCont::class,'GetLogo']);
