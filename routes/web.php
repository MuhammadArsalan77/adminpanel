<?php

use App\Http\Controllers\AdminCont;
use App\Http\Controllers\barChartController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\NotifyController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('/', [NotificationCont::class,'Index'])->middleware('auth');
//Route::post('/mark-as-read', 'NotificationCont@markNotification')->name('markNotification');
// Route::get('/email/verify', function () {
//     return view('auth.verify');
// })->middleware('auth')->name('verification.notice');

Route::get('/', [DashboardController::class,'dashboard'])->name('admin.dashboard');
Route::get('Profile', function () {
    return view('LeftNavbar.OurProfile');
})->middleware('auth');
Route::get('Email', function () {
    return view('LeftNavbar.Email');
})->middleware('auth');
Route::get('Chat', function () {
    return view('LeftNavbar.Chat');
})->middleware('auth');

Route::get('Contact', function () {
    return view('LeftNavbar.Contact');
})->middleware('auth');
Route::get('BasicForm', function () {
    return view('LeftNavbar.BasicForm');
})->middleware('auth');
Route::get('AdvancedForm', function () {
    return view('LeftNavbar.AdvancedForm');
})->middleware('auth');
Route::get('NormalTable', function () {
    return view('LeftNavbar.NormalTable');
})->middleware('auth');

Route::get('Editable', function () {
    return view('LeftNavbar.Editable');
})->middleware('auth');
Route::get('Foo', function () {
    return view('LeftNavbar.Foo');
})->middleware('auth');
Route::get('TableColor', function () {
    return view('LeftNavbar.TableColor');
})->middleware('auth');
Route::get('JuqeryTables', function () {
    return view('LeftNavbar.JqueryDataTable');
})->middleware('auth');

Auth::routes(['verify'=>true]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//Customer Record
Route::get('Rec',[AdminCont::class,'ShowData'])->middleware('auth');

//Update Customer Status(Block And Unblock)
Route::post('updateStatuss',[AdminCont::class,'status'])->name('changestatus')->middleware('auth');

//Add Pizza
Route::get('AddPizzas',[AdminCont::class,'Addpizza']);
Route::post('AddedPizzas',[AdminCont::class,'Addedpizza'])->name('AddedProduct')->middleware('auth');

//Show Pizza Tables
Route::get('ShowPizzaDetails',[AdminCont::class,'ShowPizzaDetail'])->middleware('auth');
Route::delete('delet/{id}',[AdminCont::class,'DeleteRecord'])->middleware('auth');
Route::get('edits/{id}',[AdminCont::class,'editPizza'])->middleware('auth');
Route::post('updated/{data}',[AdminCont::class,'updateData'])->middleware('auth');


Route::post('updateStatus',[AdminCont::class,'statuss'])->name('changeOrderStatus')->middleware('auth');
Route::get('Order',[AdminCont::class,'orders'])->middleware('auth');
Route::get('DeliverOrder',[AdminCont::class,'DeliveredOrder'])->middleware('auth');

Route::get('Banner',[AdminCont::class,'Banners'])->middleware('auth');
Route::post('BannerImg',[AdminCont::class,'Images'])->name('Bannerss')->middleware('auth');
Route::get('Banners',[AdminCont::class,'AllBanners'])->middleware('auth');
Route::delete('delete/{id}',[AdminCont::class,'DeleteRecs'])->middleware('auth');

//Category Form
Route::get('AddCategory',[AdminCont::class,'AddCategory'])->middleware('auth');
Route::post('AddedCategory',[AdminCont::class,'AddedCategory'])->name('AdCategory')->middleware('auth');
Route::get('ShowCategory',[AdminCont::class,'ShowCategory'])->name('show.category')->middleware('auth');
Route::delete('del/{id}',[AdminCont::class,'DeleteCategory'])->middleware('auth');
Route::get('edit/{id}',[AdminCont::class,'EditCategory'])->middleware('auth');
Route::post('UpdateCatgory/{data}',[AdminCont::class,'UpdateCatgory'])->middleware('auth');

Route::get('CoupenForm',[AdminCont::class,'addCoupen'])->middleware('auth');
Route::post('AddedCoupen',[AdminCont::class,'addedCoupen'])->name('AddedCoupen')->middleware('auth');
Route::get('CoupenData',[AdminCont::class,'CoupenData'])->name('CoupenData')->middleware('auth');

Route::get('editcopen/{id}',[AdminCont::class,'EditCopen'])->middleware('auth');
Route::post('UpdatedCoupen/{data}',[AdminCont::class,'UpdatedCoupen'])->middleware('auth');

Route::delete('dele/{id}',[AdminCont::class,'DeleteCoupen'])->middleware('auth');


//logo routes
Route::get('Logo',[AdminCont::class,'LogoImage'])->middleware('auth');
Route::get('editlogo/{id}',[AdminCont::class,'EditLogo'])->middleware('auth');
Route::post('UpdatedLogo/{data}',[AdminCont::class,'UpdatedLogo'])->middleware('auth');

//notification Route
Route::get('/new/notifications',[NotifyController::class,'newCustomerNotification'])->middleware('auth')->name('new.customer.notification');
Route::post('/mark-as-read', [NotifyController::class,'markNotification'])->middleware('auth')->name('admin.markNotification');
// Route::get('/chart',[barChartController::class,'index']);
