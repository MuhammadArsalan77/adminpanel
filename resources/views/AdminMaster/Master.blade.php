<!doctype html>
<html class="chrome" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
    <title>Admin Panel</title>
    <link rel="icon" href="favicon.ico" type="image/x-icon"> <!-- Favicon-->
    <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/plugins/dropify/css/dropify.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/plugins/sweetalert/sweetalert.css')}}"/>
    {{-- <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap/css/bootstrap.css') }}" type="text/css"> --}}
    {{-- <link rel="stylesheet" href="{{ asset('assets/plugins/jvectormap/jquery-jvectormap-2.0.3.min.css') }}" type="text/css"/> --}}
    <link rel="stylesheet" href="{{ asset('assets/plugins/charts-c3/plugin.css') }}" type="text/css"/>

    {{-- <link rel="stylesheet" href="{{ asset('assets/plugins/morrisjs/morris.min.css') }}" type="text/css"/> --}}

    <!-- Custom Css -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.min.css') }}" type="text/css"/>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css" type="text/css"  /> --}}
    {{-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/dataTables.bootstrap4.min.css" type="text/css" /> --}}
    {{-- <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.bootstrap4.min.css" type="text/css" /> --}}
    <style>
        .f-cont{
            cursor: pointer;
        font-size: 1rem;
        color: #495057;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid #ced4da;
        border-radius: .25rem;
        transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
           }
    </style>
    </head>
    <body class="theme-blush">

        <div class="page-loader-wrapper">
            <div class="loader">
                <div class="m-t-30"><img class="zmdi-hc-spin" src="{{ asset('assets/images/loader.svg') }}" width="48" height="48" alt="Aero"></div>
                <p>Please wait...</p>
            </div>
        </div>
        <!-- Overlay For Sidebars -->
        <div class="overlay"></div>
        <!-- Main Search -->
        {{-- <div id="search">
            <button id="close" type="button" class="close btn btn-primary btn-icon btn-icon-mini btn-round">x</button>
            <form>
            <input type="search" value="" placeholder="Search..." />
            <button type="submit" class="btn btn-primary">Search</button>
            </form>
        </div> --}}
        <!--Right Icon Menu Sidebar-->
        <div class="navbar-right">
            <ul class="navbar-nav">
                {{-- <li><a href="#search" class="main_search" title="Search..."><i class="zmdi zmdi-search"></i></a></li> --}}
                {{-- <li class="dropdown">
                    <a href="javascript:void(0);" class="dropdown-toggle" title="App" data-toggle="dropdown" role="button"><i class="zmdi zmdi-apps"></i></a>
                    <ul class="dropdown-menu slideUp2">
                        <li class="header">App Sortcute</li>
                        <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 330px;"><li class="body" style="overflow: hidden; width: auto; height: 330px;">
                            <ul class="menu app_sortcut list-unstyled">
                                <li>
                                    <a href="image-gallery.html">
                                        <div class="icon-circle mb-2 bg-blue"><i class="zmdi zmdi-camera"></i></div>
                                        <p class="mb-0">Photos</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">
                                        <div class="icon-circle mb-2 bg-amber"><i class="zmdi zmdi-translate"></i></div>
                                        <p class="mb-0">Translate</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="events.html">
                                        <div class="icon-circle mb-2 bg-green"><i class="zmdi zmdi-calendar"></i></div>
                                        <p class="mb-0">Calendar</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="contact.html">
                                        <div class="icon-circle mb-2 bg-purple"><i class="zmdi zmdi-account-calendar"></i></div>
                                        <p class="mb-0">Contacts</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">
                                        <div class="icon-circle mb-2 bg-red"><i class="zmdi zmdi-tag"></i></div>
                                        <p class="mb-0">News</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">
                                        <div class="icon-circle mb-2 bg-grey"><i class="zmdi zmdi-map"></i></div>
                                        <p class="mb-0">Maps</p>
                                    </a>
                                </li>
                            </ul>
                        </li><div class="slimScrollBar" style="background: rgba(0, 0, 0, 0.2); width: 3px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 3px; z-index: 99; right: 1px;"></div><div class="slimScrollRail" style="width: 3px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 0px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
                    </ul>
                </li> --}}
                <li class="dropdown">
                    <a href="javascript:void(0);" class="dropdown-toggle" title="Notifications" data-toggle="dropdown" role="button"><i class="zmdi zmdi-notifications"></i>
                        <div class="notify"><span class="heartbit"></span><span class="point"></span></div>
                    </a>
                    <ul class="dropdown-menu slideUp2">
                        <li class="header">Notifications</li>
                        <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 330px;"><li class="body" style="overflow: hidden; width: auto; height: 330px;">
                            <ul class="menu list-unstyled">
                                <li>
                                    <a href="{{ URL::route('new.customer.notification') }}">
                                        <div class="icon-circle bg-blue"><i class="zmdi zmdi-account"></i></div>
                                        <div class="menu-info">

                                            <p><i class="zmdi zmdi-time"></i> 14 mins ago </p>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">
                                        <div class="icon-circle bg-amber"><i class="zmdi zmdi-shopping-cart"></i></div>
                                        <div class="menu-info">
                                            <h4>4 Sales made</h4>
                                            <p><i class="zmdi zmdi-time"></i> 22 mins ago </p>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">
                                        <div class="icon-circle bg-red"><i class="zmdi zmdi-delete"></i></div>
                                        <div class="menu-info">
                                            <h4><b>Nancy Doe</b> Deleted account</h4>
                                            <p><i class="zmdi zmdi-time"></i> 3 hours ago </p>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">
                                        <div class="icon-circle bg-green"><i class="zmdi zmdi-edit"></i></div>
                                        <div class="menu-info">
                                            <h4><b>Nancy</b> Changed name</h4>
                                            <p><i class="zmdi zmdi-time"></i> 2 hours ago </p>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">
                                        <div class="icon-circle bg-grey"><i class="zmdi zmdi-comment-text"></i></div>
                                        <div class="menu-info">
                                            <h4><b>John</b> Commented your post</h4>
                                            <p><i class="zmdi zmdi-time"></i> 4 hours ago </p>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">
                                        <div class="icon-circle bg-purple"><i class="zmdi zmdi-refresh"></i></div>
                                        <div class="menu-info">
                                            <h4><b>John</b> Updated status</h4>
                                            <p><i class="zmdi zmdi-time"></i> 3 hours ago </p>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">
                                        <div class="icon-circle bg-light-blue"><i class="zmdi zmdi-settings"></i></div>
                                        <div class="menu-info">
                                            <h4>Settings Updated</h4>
                                            <p><i class="zmdi zmdi-time"></i> Yesterday </p>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </li><div class="slimScrollBar" style="background: rgba(0, 0, 0, 0.2); width: 3px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 3px; z-index: 99; right: 1px;"></div><div class="slimScrollRail" style="width: 3px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 0px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
                        <li class="footer"> <a href="javascript:void(0);">View All Notifications</a> </li>
                    </ul>
                </li>
                {{-- <li class="dropdown">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button"><i class="zmdi zmdi-flag"></i>
                    <div class="notify"><span class="heartbit"></span><span class="point"></span></div>
                    </a>
                    <ul class="dropdown-menu slideUp2">
                        <li class="header">Tasks List <small class="float-right"><a href="javascript:void(0);">View All</a></small></li>
                        <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 330px;"><li class="body" style="overflow: hidden; width: auto; height: 330px;">
                            <ul class="menu tasks list-unstyled">
                                <li>
                                    <div class="progress-container progress-primary">
                                        <span class="progress-badge">eCommerce Website</span>
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="86" aria-valuemin="0" aria-valuemax="100" style="width: 86%;">
                                                <span class="progress-value">86%</span>
                                            </div>
                                        </div>
                                        <ul class="list-unstyled team-info">
                                            <li class="m-r-15"><small>Team</small></li>
                                            <li>
                                                <img src="{{ asset('assets/images/xs/avatar2.jpg') }}" alt="Avatar">
                                            </li>
                                            <li>
                                                <img src="{{ asset('assets/images/xs/avatar3.jpg') }}" alt="Avatar">
                                            </li>
                                            <li>
                                                <img src="{{ asset('assets/images/xs/avatar4.jpg') }}" alt="Avatar">
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li>
                                    <div class="progress-container">
                                        <span class="progress-badge">iOS Game Dev</span>
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%;">
                                                <span class="progress-value">45%</span>
                                            </div>
                                        </div>
                                        <ul class="list-unstyled team-info">
                                            <li class="m-r-15"><small>Team</small></li>
                                            <li>
                                                <img src="{{ asset('assets/images/xs/avatar10.jpg') }}" alt="Avatar">
                                            </li>
                                            <li>
                                                <img src="{{ asset('assets/images/xs/avatar9.jpg') }}" alt="Avatar">
                                            </li>
                                            <li>
                                                <img src="{{ asset('assets/images/xs/avatar8.jpg') }}" alt="Avatar">
                                            </li>
                                            <li>
                                                <img src="{{ asset('assets/images/xs/avatar7.jpg') }}" alt="Avatar">
                                            </li>
                                            <li>
                                                <img src="{{ asset('assets/images/xs/avatar6.jpg') }}" alt="Avatar">
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li>
                                    <div class="progress-container progress-warning">
                                        <span class="progress-badge">Home Development</span>
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="29" aria-valuemin="0" aria-valuemax="100" style="width: 29%;">
                                                <span class="progress-value">29%</span>
                                            </div>
                                        </div>
                                        <ul class="list-unstyled team-info">
                                            <li class="m-r-15"><small>Team</small></li>
                                            <li>
                                                <img src="{{ asset('assets/images/xs/avatar5.jpg') }}" alt="Avatar">
                                            </li>
                                            <li>
                                                <img src="{{ asset('assets/images/xs/avatar2.jpg') }}" alt="Avatar">
                                            </li>
                                            <li>
                                                <img src="{{ asset('assets/images/xs/avatar7.jpg') }}" alt="Avatar">
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </li><div class="slimScrollBar" style="background: rgba(0, 0, 0, 0.2); width: 3px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 3px; z-index: 99; right: 1px;"></div><div class="slimScrollRail" style="width: 3px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 0px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
                    </ul>
                </li>
                <li><a href="javascript:void(0);" class="app_calendar" title="Calendar"><i class="zmdi zmdi-calendar"></i></a></li>
                <li><a href="javascript:void(0);" class="app_google_drive" title="Google Drive"><i class="zmdi zmdi-google-drive"></i></a></li>
                <li><a href="javascript:void(0);" class="app_group_work" title="Group Work"><i class="zmdi zmdi-group-work"></i></a></li> --}}

                <li><a href="javascript:void(0);" class="js-right-sidebar" title="Setting"><i class="zmdi zmdi-settings zmdi-hc-spin"></i></a></li>
                <li><a href="{{ route('logout') }}" class="mega-menu" title="Sign Out"  onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();"><i class="zmdi zmdi-power"></i></a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </li>
            </ul>
        </div>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <div class="navbar-brand">
                <button class="btn-menu ls-toggle-btn" type="button"><i class="zmdi zmdi-menu"></i></button>
                <a href="#"><img src="{{ asset('assets/images/logo.svg') }}" width="25" alt="Aero"><span class="m-l-10">Aero</span></a>
            </div>
            <div class="menu">
                <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: calc(100vh - 65px);"><ul class="list" style="overflow: hidden; width: auto; height: calc(100vh - 65px);">
                    <li>
                        <div class="user-info">
                            <a class="image waves-effect waves-block" href="profile.html"><img src="{{ asset('assets/images/profile_av.jpg') }}" alt="User"></a>
                            <div class="detail">
                                <h4></h4>
                                <small>{{ Auth::user()->name }}</small>
                            </div>
                        </div>
                    </li>
                    <li><a href="{{ URL::route('admin.dashboard') }}" class=" waves-effect waves-block"><i class="zmdi zmdi-home"></i><span>Dashboard</span></a></li>
                    <li class="active open"><a href="{{ URL::to('Profile') }}" class="toggled waves-effect waves-block"><i class="zmdi zmdi-account"></i><span>Our Profile</span></a></li>

                    <li><a href="javascript:void(0);" class="menu-toggle waves-effect waves-block"><i class="zmdi zmdi-lock"></i><span>Logo</span></a>
                        <ul class="ml-menu">
                            <li><a href="{{ URL::to('Logo') }}" class=" waves-effect waves-block">Update Logo</a></li>
                        </ul>
                    </li>
                    <li><a href="javascript:void(0);" class="menu-toggle waves-effect waves-block"><i class="zmdi zmdi-assignment"></i><span>Pizza</span></a>
                        <ul class="ml-menu">
                            <li><a href="{{ URL::to('AddPizzas') }}" class=" waves-effect waves-block"> Add Pizza Detail</a></li>
                            <li><a href="{{ URL::to('ShowPizzaDetails') }}" class=" waves-effect waves-block">Pizza Details</a></li>
                        </ul>
                    </li>
                    <li><a href="javascript:void(0);" class="menu-toggle waves-effect waves-block"><i class="zmdi zmdi-assignment"></i><span> Banners</span></a>
                        <ul class="ml-menu">
                            <li><a href="{{ URL::to('Banner') }}" class=" waves-effect waves-block">Add Banner</a></li>
                            <li><a href="{{ URL::to('Banners') }}" class=" waves-effect waves-block"> Show Banners</a></li>
                            <li><a></a></li>
                        </ul>
                    </li>
                    <li><a href="javascript:void(0);" class="menu-toggle waves-effect waves-block"><i class="zmdi zmdi-grid"></i><span>Customer</span></a>
                        <ul class="ml-menu">
                            {{-- <li><a href="{{ URL::to('NormalTable') }}" class=" waves-effect waves-block">Normal Tables</a></li> --}}
                            <li><a href="{{ URL::to('Rec') }}" class=" waves-effect waves-block">Customer Record</a></li>
                            {{-- <li><a href="{{ URL::to('Editable') }}" class=" waves-effect waves-block">Editable Tables</a></li> --}}
                            {{-- <li><a href="{{ URL::to('ShowPizzaDetails') }}" class=" waves-effect waves-block">Pizza Details</a></li> --}}
                            {{-- <li><a href="{{ URL::to('TableColor') }}" class=" waves-effect waves-block">Tables Color</a></li> --}}
                        </ul>
                    </li>

                    </li>
                    {{-- <li><a href="javascript:void(0);" class="menu-toggle waves-effect waves-block"><i class="zmdi zmdi-chart"></i><span>Charts</span></a>
                        <ul class="ml-menu">
                            <li><a href="echarts.html" class=" waves-effect waves-block">E Chart</a></li>
                            <li><a href="c3.html" class=" waves-effect waves-block">C3 Chart</a></li>
                            <li><a href="morris.html" class=" waves-effect waves-block">Morris</a></li>
                            <li><a href="flot.html" class=" waves-effect waves-block">Flot</a></li>
                            <li><a href="chartjs.html" class=" waves-effect waves-block">ChartJS</a></li>
                            <li><a href="sparkline.html" class=" waves-effect waves-block">Sparkline</a></li>
                            <li><a href="jquery-knob.html" class=" waves-effect waves-block">Jquery Knob</a></li>
                        </ul>
                    </li>
                    <li><a href="javascript:void(0);" class="menu-toggle waves-effect waves-block"><i class="zmdi zmdi-delicious"></i><span>Widgets</span></a>
                        <ul class="ml-menu">
                            <li><a href="widgets-app.html" class=" waves-effect waves-block">Apps Widgets</a></li>
                            <li><a href="widgets-data.html" class=" waves-effect waves-block">Data Widgets</a></li>
                        </ul>
                    </li> --}}
                     <li><a href="javascript:void(0);" class="menu-toggle waves-effect waves-block"><i class="zmdi zmdi-lock"></i><span>Orders</span></a>
                        <ul class="ml-menu">
                            <li><a href="{{ URL::to('Order') }}" class=" waves-effect waves-block">Order Details</a></li>
                            <li><a href="{{ URL::to('DeliverOrder') }}" class=" waves-effect waves-block"> Delivered Orders</a></li>
                        </ul>
                    </li>
                    <li><a href="javascript:void(0);" class="menu-toggle waves-effect waves-block"><i class="zmdi zmdi-assignment"></i><span>Category</span></a>
                        <ul class="ml-menu">
                            <li><a href="{{ URL::to('AddCategory') }}" class=" waves-effect waves-block">Add Category</a></li>
                            <li><a href="{{ URL::to('ShowCategory') }}" class=" waves-effect waves-block">Show Category</a></li>
                        </ul>
                    </li>
                    <li><a href="javascript:void(0);" class="menu-toggle waves-effect waves-block"><i class="zmdi zmdi-grid"></i><span>Coupen</span></a>
                        <ul class="ml-menu">
                            {{-- <li><a href="{{ URL::to('NormalTable') }}" class=" waves-effect waves-block">Normal Tables</a></li> --}}
                            <li><a href="{{ URL::to('CoupenForm') }}" class=" waves-effect waves-block">Coupen Form</a></li>
                            <li><a href="{{ URL::to('CoupenData') }}" class=" waves-effect waves-block">Coupen Data</a></li>
                        </ul>
                    </li>
            </div>
        </aside>
        <!-- Right Sidebar -->
        <aside id="rightsidebar" class="right-sidebar">
            <ul class="nav nav-tabs sm">
                <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#setting"><i class="zmdi zmdi-settings zmdi-hc-spin"></i></a></li>
                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#chat"><i class="zmdi zmdi-comments"></i></a></li>
            </ul>

        </aside>


@yield('child')
@yield('script')
{{-- <script src="{{asset('assets/js/pages/forms/dropify.js')}}"></script> --}}
<script src="{{asset('assets/plugins/dropify/js/dropify.min.js')}}"></script>


<script src="{{asset('assets/plugins/sweetalert/sweetalert.min.js')}}"></script>
<script src="{{asset('assets/js/pages/ui/sweetalert.js')}}"></script>

{{-- <script async="" src="https://static-v.tawk.to/709/app.js" charset="UTF-8" crossorigin="*"></script> --}}
{{-- <script async="" src="https://embed.tawk.to/5c6d4867f324050cfe342c69/default" charset="UTF-8" crossorigin="*"></script> --}}

<script src="{{ asset('assets/bundles/libscripts.bundle.js') }}"></script> <!-- Lib Scripts Plugin Js ( jquery.v3.2.1, Bootstrap4 js) -->
<script src="{{ asset('assets/bundles/vendorscripts.bundle.js') }}"></script> <!-- slimscroll, waves Scripts Plugin Js -->

{{-- <script src="{{ asset('assets/bundles/jvectormap.bundle.js') }}"></script> <!-- JVectorMap Plugin Js --> --}}
{{-- <script src="{{ asset('assets/bundles/sparkline.bundle.js') }}"></script> <!-- Sparkline Plugin Js --> --}}
<script src="{{ asset('assets/bundles/c3.bundle.js') }}"></script>
<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>

{{-- <script src="{{ asset('assets/js/pages/index.js') }}"></script> --}}

    </body>
    <!-- Jquery Core Js -->
    <div class="jvectormap-tip"></div>


</html>

