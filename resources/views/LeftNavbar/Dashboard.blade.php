@extends('AdminMaster.Master')
@section('child')
    <!-- Main Content -->
    <section class="content">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-7 col-md-6 col-sm-12">
                    <h2>Dashboard</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ URL::to('/') }}"><i class="zmdi zmdi-home"></i> Pizza</a>
                        </li>
                        <li class="breadcrumb-item active">Dashboard </li>
                    </ul>
                    {{-- <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button> --}}
                </div>
                <div class="col-lg-5 col-md-6 col-sm-12">
                    <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i
                            class="zmdi zmdi-arrow-right"></i></button>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <a href="Rec">
                        <div class="card widget_2 big_icon traffic">
                            <div class="body">
                                <h6>User</h6>
                                <h2>{{ $customer }} </h2>
                                <small>Total Users</small>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <a href="Order">
                        <div class="card widget_2 big_icon sales">
                            <div class="body">
                                <h6>Orders</h6>
                                <h2>{{ $order }}</h2>
                                <small>Total Orders</small>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <a href="{{ URL::route('show.category') }}">
                        <div class="card widget_2 big_icon email">
                            <div class="body">
                                <h6>Category</h6>
                                <h2>{{ $category }}</h2>
                                <small>Total Categories</small>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="card">
                        <div class="header">
                            <h2><strong> User </strong>Bar Chart</h2>
                            <ul class="header-dropdown">
                                <li class="remove">
                                    <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <canvas id="barChart" class="chartjs_graph"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div>

            </div>

        </div>
    </section>

    <!--End Main Content -->
@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<script>
    $(function() {
        var datas=<?php echo json_encode($datas); ?>;
        var barCanvas= $("#barChart");
        var barChart = new Chart(barCanvas,{
            type: 'bar',
            data:{
                labels:['Jan','Feb','Mar','Apr','May','Jun','July','Aug','Sep','Oct','Nov','Dec'],
                datasets:[
                    {
                        label: 'New User Growth 2021',
                        data:datas,
                        backgroundColor:['red','green','blue','yellow','pink','violet','silver','brown','orange','gold','purple','indigo']
                    }
                ]
            },
            options:{
                scales:{
                    yAxes:[{
                        ticks:{
                            beginAtZero:true
                        }
                    }]
                }
            }
        })
    });

</script>
@endsection
