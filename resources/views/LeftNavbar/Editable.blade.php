@extends('AdminMaster.Master')
@section('child')
<section class="content">
    <div class="body_scroll">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-7 col-md-6 col-sm-12">
                    <h2>Editable Tables</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i> Aero</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Tables</a></li>
                        <li class="breadcrumb-item active">Editable Tables</li>
                    </ul>
                    <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
                </div>
                <div class="col-lg-5 col-md-6 col-sm-12">
                    <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i class="zmdi zmdi-arrow-right"></i></button>
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <p>You can edit any columns except header/footer</p>
                        <table id="mainTable" class="table table-striped c_table" style="cursor: pointer;">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Cost</th>
                                    <th>Profit</th>
                                    <th>Fun</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td tabindex="1">Car</td>
                                    <td tabindex="1">100</td>
                                    <td tabindex="1">200</td>
                                    <td tabindex="1">0</td>
                                </tr>
                                <tr>
                                    <td tabindex="1">Bike</td>
                                    <td tabindex="1">330</td>
                                    <td tabindex="1">240</td>
                                    <td tabindex="1">1</td>
                                </tr>
                                <tr>
                                    <td tabindex="1">Plane</td>
                                    <td tabindex="1">430</td>
                                    <td tabindex="1">540</td>
                                    <td tabindex="1">3</td>
                                </tr>
                                <tr>
                                    <td tabindex="1">Yacht</td>
                                    <td tabindex="1">100</td>
                                    <td tabindex="1">200</td>
                                    <td tabindex="1">0</td>
                                </tr>
                                <tr>
                                    <td tabindex="1">Segway</td>
                                    <td tabindex="1">330</td>
                                    <td tabindex="1">240</td>
                                    <td tabindex="1">1</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th><strong>TOTAL</strong></th>
                                    <th>1290</th>
                                    <th>1420</th>
                                    <th>5</th>
                                </tr>
                            </tfoot>
                        </table>
                    <input style="position: absolute; display: none;"></div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
