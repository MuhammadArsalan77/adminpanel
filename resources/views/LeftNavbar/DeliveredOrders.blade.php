@extends('AdminMaster.Master')
@section('child')

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha256-4+XzXVhsDmqanXGHaHvgh1gMQKX40OUvDEBTu8JcmNs=" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.23/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.bootstrap4.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.colVis.min.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css" type="text/css"  />
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/dataTables.bootstrap4.min.css" type="text/css" />
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.bootstrap4.min.css" type="text/css" />

<link rel="stylesheet" href="{{asset('assets/plugins/dropify/css/dropify.min.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/sweetalert/sweetalert.css')}}"/>
<script src="{{asset('assets/plugins/dropify/js/dropify.min.js')}}"></script>

<script src="{{asset('assets/plugins/sweetalert/sweetalert.min.js')}}"></script>
<script src="{{asset('assets/js/pages/ui/sweetalert.js')}}"></script>
<section class="content">
    <div class="body_scroll">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-7 col-md-6 col-sm-12">
                    {{-- <h2>Form Examples</h2> --}}
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ URL::to('/') }}"><i class="zmdi zmdi-home"></i> Dashboard</a></li>
                        <li class="breadcrumb-item active"><a href="javascript:void(0);">Order</a></li>

                    </ul>
                    <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
                </div>
                <div class="col-lg-5 col-md-6 col-sm-12">
                    <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i class="zmdi zmdi-arrow-right"></i></button>
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <!-- Vertical Layout -->

            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="header">

                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover" id="table" >
                                    <thead>
                                        <tr>
                                            <th>Order ID</th>
                                            <th>Special Note</th>
                                            <th>Total Price</th>
                                            <th>Order Status</th>
                                            <th>Address</th>
                                            <th>Special Note</th>
                                            <th>Customer Name</th>
                                            <th>Customer Contact</th>
                                            <th>Customer Email</th>
                                            <th>Order Details</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Order ID</th>
                                            <th>Special Note</th>
                                            <th>Total Price</th>

                                            <th>Address</th>
                                            <th>Order Type</th>
                                            <th>Customer Name</th>
                                            <th>Customer Contact</th>
                                            <th>Customer Email</th>
                                            <th>Order Details</th>

                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                         @foreach ($data as $value)
                                    @if($value->active_status == 1)
                                        <tr id="cid{{ $value->id }}">
                                            <td>{{ $value->id }}</td>
                                            <td>{{ $value->special_note }}</td>
                                            <td>{{ $value->total_amount }}</td>
                                            <td>
                                               @if($value->active_status == 1)
                                                <a id="#">
                                                    <span class="badge badge-success text-white" >Delivered</span>
                                                </a>
                                                    @endif
                                            </td>
                                            <td>{{ $value->Address }}</td>
                                            <td>
                                                 @if($value->order_type == "takeAway")
                                                 <b class="badge badge-success">Take Away</b>
                                                 @else
                                                    <b class="badge badge-danger">Delivery</b>
                                                 @endif
                                            </td>
                                            <td>
                                                {{ $value->customer_name }}
                                            </td>
                                             <td>
                                                {{ $value->customer_contact }}
                                            </td>
                                             <td>
                                                {{ $value->customer_email }}
                                            </td>
                                            <td>

                                              @php
                                                $items = json_decode($value->tdata);
                                              @endphp
                                                <table class="table table-hovered">
                                               @for($i = 0; $i<count($items); $i++)
                                                    <tr>
                                                    @foreach ($items[$i] as $key => $part)
                                                        <td>
                                                            @if($key == "pizza_id")
                                                                <b>ID</b>
                                                            @endif
                                                             @if($key == "pizza_description")
                                                                <b>Description</b>
                                                            @endif

                                                             @if($key == "pizza_name")
                                                                <b> Name</b>
                                                            @endif
                                                             @if($key == "selectedSize")

                                                                 <b> Size</b>
                                                            @endif
                                                             @if($key == "selectedSizePrice")
                                                               <b> Price</b>
                                                            @endif

                                                        </td>
                                                        <td>{{$part }}</td>
                                                    @endforeach
                                                    </tr>
                                               @endfor
                                               </table>
                                            </td>

                                            <td></td>

                                      </tr>
                                @endif
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>

</section>
<script>
  $('#table').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            {
           extend: 'pdf',
           title: 'Pending Orders ',
            pageSize: 'A4',
            filename: "Pending Orders",
           footer: false,
  customize: function(doc) {
    doc.styles.title = {
      color: 'red',
      fontSize: '20',
      alignment: 'center'
    },
    doc.defaultStyle.fontSize = 12;
    doc.defaultStyle.alignment='center';
    doc.styles['td.bigcol'] = {
                        fontSize: 16,
                        bold: true,

                    },

    doc.content[1].table.widths = [
        '10%',
        '10%',
        '20%',
        '10%',
        '20%',
        '20%',
        '10%',

],
doc.content[1].margin = [ 10, 0, 10, 0 ]
  },

        pageSize: 'LEGAL',
           exportOptions: {
                columns: [0,1,2,3,4,5,6],
                modifier: {
                page: 'current'
            }
            }
       },
       {

           extend: 'csv',
           footer: false,
           exportOptions: {
                columns: [0,1,2,3,4,5,6]
            },
       },
       {
           extend: 'excel',
           footer: false,
           exportOptions: {
                columns: [0,1,2,3,4,5,6]
            },
       }
                        ]
                    });
   </script>
<script>
     function changeStatus(id) {
        swal("Change Order Status from here?", {
            buttons: {
            cancel: "Cancel",
            catch: {
                text: "Delivered",
                value: "catch",
            },
            defeat: "pending",
            },
        })
        .then((value) => {
            switch (value) {

            case "defeat":
            changeStatusInDb(id,0);
                break;

            case "catch":
            changeStatusInDb(id,1);
                break;

            default:
                console.log("cancel");
            }
        });
    }
     function changeStatusInDb(id,active_status){
        $.ajax({
			method: 'POST',
            url: "{{route('changeOrderStatus')}}",
            data: {id:id,_token: "{{ csrf_token() }}",active_status:active_status},
            success: function(response) {
                console.log(response);
                if(response.status){
                    if(active_status == 0){
                        console.log(status);
                        swal("Success!", " Order Not Delivered!", "error");
                        var spanTag =  $("#status"+id).children("span");
                        console.log(spanTag);
                        spanTag.removeClass('badge-success').addClass("badge-warning text-white").text('Pending');
                    }else
                    {
                        console.log(status);
                        var spanTag =  $("#status"+id).children("span");
                        console.log(spanTag);
                        spanTag.removeClass('badge-danger').addClass("badge-success text-white").text('Delivered');
                        swal("Success!", "Order Delivered Successfully!", "success");
                    }
            }else{
                swal("Error!", "Unexpected Error Occur!", "error");
            }
            }
        });
    }
</script>
@endsection
