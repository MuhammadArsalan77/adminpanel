@extends('AdminMaster.Master')
@section('child')
    <!-- Main Content -->
    <section class="content">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-7 col-md-6 col-sm-12">
                    <h2>New Customer Notifications</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ URL::to('/') }}"><i class="zmdi zmdi-home"></i> Pizza</a>
                        </li>
                        <li class="breadcrumb-item active">NewCustomerNotifications </li>
                    </ul>
                    {{-- <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button> --}}
                </div>
                <div class="col-lg-5 col-md-6 col-sm-12">
                    <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i
                            class="zmdi zmdi-arrow-right"></i></button>
                </div>
            </div>
        </div>
        <form>
            @forelse ($notifications as $notification)
                <div class="alert alert-success" role="alert">
                    {{ $notification->created_at }} User {{ $notification->data['first_name'] }}
                    {{ $notification->data['last_name'] }} has just registered.
                    @csrf
                    <a href="#" class="float-right mark-as-read" data-id="{{ $notification->id }}">Mark as read</a>
                </div>
                @if ($loop->last)
                    <a href="#" class="mark-all" id="mark-all"> Mark All As Read </a>
                @endif
            @empty
                There are no new notifications
            @endforelse
        </form>
    </section>

    <!--End Main Content -->
@endsection
@section('script')
<script>
    function sendMarkRequest(id = null) {
        return $.ajax("{{ route('admin.markNotification') }}", {
            method: 'POST',
            data: {
                _token: $("input[name=_token]").val(),
                id: id
            }
        });
    }
    $(function() {

        $('.mark-as-read').click(function() {
            let request = sendMarkRequest($(this).data('id'));
            console.log(request);
            request.done(() => {
                $(this).parents('div.alert').remove();

            });
        });

        $('#mark-all').click(function() {
            let request = sendMarkRequest();

            request.done(() => {
                $('div.alert').remove();
                $('#mark-all').hide();
            })
        });
    });

</script>
@endsection
