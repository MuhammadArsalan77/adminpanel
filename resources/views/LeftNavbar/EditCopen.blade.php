@extends('AdminMaster.Master')
@section('child')

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha256-4+XzXVhsDmqanXGHaHvgh1gMQKX40OUvDEBTu8JcmNs=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="{{asset('assets/plugins/dropify/css/dropify.min.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/sweetalert/sweetalert.css')}}"/>
<script src="{{asset('assets/plugins/dropify/js/dropify.min.js')}}"></script>

<script src="{{asset('assets/plugins/sweetalert/sweetalert.min.js')}}"></script>
<script src="{{asset('assets/js/pages/ui/sweetalert.js')}}"></script>


<section class="content">
    <div class="body_scroll">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-7 col-md-6 col-sm-12">
                    <h2>Add CoupenCode Form</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ URL::to('/') }}"><i class="zmdi zmdi-home"></i> Dashborad</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Edit Coupen</a></li>

                    </ul>
                </div>
                <div class="col-lg-5 col-md-6 col-sm-12">
                    <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i class="zmdi zmdi-arrow-right"></i></button>
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <form method="POST" action="{{ URL::to('UpdatedCoupen/'.$data->id) }}">
                @csrf

            <!-- Input -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="body">
                            <h3 class="" style="text-align: center"> Coupen Form </h3>

                            <div class="row clearfix">
                                <div class="col-sm-12">
                                    <h2 class="card-inside-title">Coupen Code</h2>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Enter Coupen Code" name="coupen_Codee" value="{{ $data->coupen_Codee }}">
                                        <span style="color: red">@error('coupen_Codee')
                                            {{ $message }}
                                        @enderror</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                    <h2 class="card-inside-title">Discount Percentage</h2>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Enter Discount Percentage" name="DiscountPercent" value="{{ $data->DiscountPercent }}">
                                        <span style="color: red">@error('DiscountPercent')
                                            {{ $message }}
                                        @enderror</span>
                                    </div>
                                </div>
                            </div>

                            <br>

                            <button type="submit" class="btn btn-raised btn-primary btn-round  waves-effect " >Add Discout</button>

                        </div>
                    </div>
                </div>
            </div>
            </form>
            @if (Session()->has('message'))
        <script>
            swal("Success!", "Coupen Data Successfully Added!", "success");
        </script>
        @endif
        </div>
    </div>
</section>
@endsection
