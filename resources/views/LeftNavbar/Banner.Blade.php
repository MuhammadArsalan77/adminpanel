@extends('AdminMaster.Master')
@section('child')





<section class="content">
    <div class="body_scroll">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-7 col-md-6 col-sm-12">
                    <h2>Add Banners</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ URL::to('/') }}"><i class="zmdi zmdi-home"></i> Dashborad</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Add Banners</a></li>

                    </ul>
                </div>
                <div class="col-lg-5 col-md-6 col-sm-12">
                    <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i class="zmdi zmdi-arrow-right"></i></button>
                </div>
            </div>
        </div>
        @if (Session()->has('message'))
        <script>
            swal("Success!", "Banner Successfully Added!", "success");
        </script>
        @endif
        <div class="container-fluid">
            <form method="POST" action="{{ route('Bannerss') }}" enctype="multipart/form-data" class="dropzone">
                @csrf
            <!-- Input -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="body">
                            {{-- <h3 class="" style="text-align: center"> Add Banner</h3> --}}

                            <div class="" style="width:50%">
                                    <h2 class="card-inside-title">Image</h2>
                                <div class="body">
                                    <div class="dropify-wrapper">
                                        <div class="dropify-message">
                                            <span class="file-icon"></span>
                                            <p>Drag and drop a file here or click</p>
                                            <p class="dropify-error">Ooops, something wrong appended.</p>
                                        </div>
                                        <div class="dropify-loader" style="display: none;">
                                            </div><div class="dropify-errors-container">

                                                </div>
                                                <input type="file" class="dropify" name="Images" >
                                                <button type="button" class="dropify-clear">Remove</button>
                                                 <div class="dropify-preview" style="display: none;">
                                                    <span class="dropify-render">
                                                    </span>
                                                    <div class="dropify-infos">
                                                        <div class="dropify-infos-inner">
                                                            <p class="dropify-filename">
                                                                <span class="file-icon"></span>
                                                                <span class="dropify-filename-inner"></span>
                                                            </p>
                                                            <p class="dropify-infos-message">Drag and drop or click to replace</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                </div>
                            </div>
                            <br>

                            <button type="submit" class="btn btn-raised btn-primary btn-round  waves-effect " >Add Banner</button>

                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
</section>
{{-- <script src="{{asset('assets/bundles/libscripts.bundle.js')}}"></script> --}}
{{-- <script src="{{asset('assets/bundles/mainscripts.bundle.js')}}"></script> --}}
{{-- <script src="{{asset('assets/bundles/vendorscripts.bundle.js')}}"></script> --}}

@endsection


