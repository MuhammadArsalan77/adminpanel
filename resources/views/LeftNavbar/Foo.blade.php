@extends('AdminMaster.Master')
@section('child')
<section class="content">
    <div class="body_scroll">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-7 col-md-6 col-sm-12">
                    <h2>Foo Tables</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i> Aero</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Tables</a></li>
                        <li class="breadcrumb-item active">Foo Tables</li>
                    </ul>
                    <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
                </div>
                <div class="col-lg-5 col-md-6 col-sm-12">
                    <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i class="zmdi zmdi-arrow-right"></i></button>
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <!-- Basic Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-striped m-b-0 footable footable-1 footable-paging footable-paging-center breakpoint-lg" style="">
                                    <thead>
                                        <tr class="footable-header">





                                        <th class="footable-sortable footable-first-visible" style="display: table-cell;">First Name<span class="fooicon fooicon-sort"></span></th><th data-breakpoints="xs" class="footable-sortable" style="display: table-cell;">Last Name<span class="fooicon fooicon-sort"></span></th><th data-breakpoints="xs" class="footable-sortable" style="display: table-cell;">Job Title<span class="fooicon fooicon-sort"></span></th><th data-breakpoints="xs" class="footable-sortable" style="display: table-cell;">BOB<span class="fooicon fooicon-sort"></span></th><th class="footable-sortable footable-last-visible" style="display: table-cell;">Status<span class="fooicon fooicon-sort"></span></th></tr>
                                    </thead>
                                        <tbody>



















                                    <tr>





                                        <td class="footable-first-visible" style="display: table-cell;">Tiger Nixon</td><td style="display: table-cell;">System Architect</td><td style="display: table-cell;">Edinburgh</td><td style="display: table-cell;">61</td><td class="footable-last-visible" style="display: table-cell;"><span class="tag tag-danger"> Suspended</span>
                                            </td></tr><tr>





                                        <td class="footable-first-visible" style="display: table-cell;">Garrett</td><td style="display: table-cell;">Accountant</td><td style="display: table-cell;">Tokyo</td><td style="display: table-cell;">63</td><td class="footable-last-visible" style="display: table-cell;"><span class="tag tag-danger"> Active</span>
                                            </td></tr><tr>





                                        <td class="footable-first-visible" style="display: table-cell;">Cox</td><td style="display: table-cell;">Author</td><td style="display: table-cell;">San</td><td style="display: table-cell;">66</td><td class="footable-last-visible" style="display: table-cell;"><span class="tag tag-default">Disabled</span>
                                            </td></tr><tr>





                                        <td class="footable-first-visible" style="display: table-cell;">Kelly</td><td style="display: table-cell;">Brielle</td><td style="display: table-cell;">Edinburgh</td><td style="display: table-cell;">22</td><td class="footable-last-visible" style="display: table-cell;"><span class="tag tag-success">Active</span>
                                            </td></tr><tr>





                                        <td class="footable-first-visible" style="display: table-cell;">Airi Satou</td><td style="display: table-cell;">Accountant</td><td style="display: table-cell;">Tokyo</td><td style="display: table-cell;">33</td><td class="footable-last-visible" style="display: table-cell;"><span class="tag tag-success"> Active</span>
                                            </td></tr><tr>





                                        <td class="footable-first-visible" style="display: table-cell;">Brielle</td><td style="display: table-cell;">Specialist</td><td style="display: table-cell;">New York</td><td style="display: table-cell;">61</td><td class="footable-last-visible" style="display: table-cell;"><span class="tag tag-default">Disabled</span>
                                            </td></tr><tr>





                                        <td class="footable-first-visible" style="display: table-cell;">Herrod Chandler</td><td style="display: table-cell;">Sales Assistant</td><td style="display: table-cell;">San</td><td style="display: table-cell;">59</td><td class="footable-last-visible" style="display: table-cell;"><span class="tag tag-danger">Suspended</span>
                                            </td></tr><tr>





                                        <td class="footable-first-visible" style="display: table-cell;">Rhona Davidson</td><td style="display: table-cell;">Specialist</td><td style="display: table-cell;">Tokyo</td><td style="display: table-cell;">55</td><td class="footable-last-visible" style="display: table-cell;"><span class="tag tag-success"> Active</span>
                                            </td></tr><tr>





                                        <td class="footable-first-visible" style="display: table-cell;">Colleen Hurst</td><td style="display: table-cell;">Javascript Developer</td><td style="display: table-cell;">San</td><td style="display: table-cell;">39</td><td class="footable-last-visible" style="display: table-cell;"><span class="tag tag-default">Disabled</span>
                                            </td></tr><tr>





                                        <td class="footable-first-visible" style="display: table-cell;">Sonya Frost</td><td style="display: table-cell;">Software</td><td style="display: table-cell;">Edinburgh</td><td style="display: table-cell;">23</td><td class="footable-last-visible" style="display: table-cell;"><span class="tag tag-danger">Suspended</span>
                                            </td></tr></tbody>
                                <tfoot><tr class="footable-paging"><td colspan="5"><ul class="pagination"><li class="footable-page-nav disabled" data-page="first"><a class="footable-page-link" href="#">«</a></li><li class="footable-page-nav disabled" data-page="prev"><a class="footable-page-link" href="#">‹</a></li><li class="footable-page visible active" data-page="1"><a class="footable-page-link" href="#">1</a></li><li class="footable-page visible" data-page="2"><a class="footable-page-link" href="#">2</a></li><li class="footable-page-nav" data-page="next"><a class="footable-page-link" href="#">›</a></li><li class="footable-page-nav" data-page="last"><a class="footable-page-link" href="#">»</a></li></ul><div class="divider"></div><span class="label label-default">1 of 2</span></td></tr></tfoot></table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
