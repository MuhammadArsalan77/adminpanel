@extends('AdminMaster.Master')
@section('child')



 <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha256-4+XzXVhsDmqanXGHaHvgh1gMQKX40OUvDEBTu8JcmNs=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="{{asset('assets/plugins/dropify/css/dropify.min.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/sweetalert/sweetalert.css')}}"/>
<script src="{{asset('assets/plugins/dropify/js/dropify.min.js')}}"></script>

<script src="{{asset('assets/plugins/sweetalert/sweetalert.min.js')}}"></script>
<script src="{{asset('assets/js/pages/ui/sweetalert.js')}}"></script>


<section class="content">
    <div class="body_scroll">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-7 col-md-6 col-sm-12">
                    <h2>Add Pizza Form</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ URL::to('/') }}"><i class="zmdi zmdi-home"></i> Dashborad</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Add Pizza</a></li>

                    </ul>
                </div>
                <div class="col-lg-5 col-md-6 col-sm-12">
                    <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i class="zmdi zmdi-arrow-right"></i></button>
                </div>
            </div>
        </div>
        @if (Session()->has('message'))
        <script>
            swal("Success!", "Record Successfully Added!", "success");
        </script>
        @endif
        <div class="container-fluid">
            <form method="POST" action="{{ route('AddedProduct') }}" enctype="multipart/form-data" class="dropzone dz-clickable" id="image-upload">
                @csrf

            <!-- Input -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="body">
                            <h3 class="" style="text-align: center"> Add Pizza Detail</h3>

                            <div class="row clearfix">
                                <div class="col-sm-6">
                                    <h2 class="card-inside-title">Pizza Name</h2>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Enter pizza Name" name="pizza_name">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <h2 class="card-inside-title"> Pizza Code</h2>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Enter pizza code" name="deal_code">
                                    </div>
                                </div>
                            </div>


                            <div class="row clearfix">
                                <div class="col-sm-6">
                                    <h2 class="card-inside-title"> Pizza price Small </h2>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Enter pizza Small" name="pizza_price_small">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <h2 class="card-inside-title"> Pizza price Medium</h2>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Enter pizza Medium" name="pizza_price_medium">
                                    </div>
                                </div>
                            </div>


                            <div class="row clearfix">
                                <div class="col-sm-6">
                                    <h2 class="card-inside-title"> Pizza price Large</h2>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Enter pizza price Large" name="pizza_price_large">
                                    </div>
                                </div>
                                <div class="col-sm-6">

                                    <h2 class="card-inside-title"> Pizza price Extra Large</h2>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Enter pizza price Extra large" name="pizza_price_xl">
                                    </div>
                                </div>
                            </div>


                            <div class="row clearfix">
                                <div class="col-sm-12">
                                    <h2 class="card-inside-title">Category</h2>
                                    <div class="">
                                        <select class="form-control" id="select_category" name="category_id" required focus>
                                            <option value="" disabled selected>select Category</option>
                                            @foreach($data as $user)
                                            <option value="{{$user->id}}">{{ $user->catory_name }}</option>
                                            @endforeach
                                          </select>
                                    </div>
                                </div>
                            </div>


                            <div class="row clearfix">
                                <div class="col-sm-12">
                                        <h2 class="card-inside-title">Description</h2>
                                        <div class="row clearfix">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <textarea rows="4" class="form-control no-resize" placeholder="Please type Description of Product..." name="pizza_description"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                            {{-- <div class="row clearfix">
                                <div class="col-sm-12">
                                    <div class="card">
                                        <diV class="card-body">
                                            <h4 class="text-center">Upload Images By Click On Box</h4>
                                            <div class="dz-default dz-message">
                                            <span>Drop Files Here to Upload</span>
                                        </div>
                                    </div>
                                <div>
                            </diV>
                            </div> --}}



                            <div class="" style="width:50%">
                                    <h2 class="card-inside-title">Image</h2>
                                <div class="body">
                                    <div class="dropify-wrapper">
                                        <div class="dropify-message">
                                            <span class="file-icon"></span>
                                            <p>Drag and drop a file here or click</p>
                                            <p class="dropify-error">Ooops, something wrong appended.</p>
                                        </div>
                                        <div class="dropify-loader" style="display: none;">
                                            </div><div class="dropify-errors-container">

                                                </div>
                                                <input type="file" class="dropify" name="pizza_Image" >
                                                <button type="button" class="dropify-clear">Remove</button>
                                                 <div class="dropify-preview" style="display: none;">

                                                    <span class="dropify-render">

                                                    </span>
                                                    <div class="dropify-infos">
                                                        <div class="dropify-infos-inner">
                                                            <p class="dropify-filename">
                                                                <span class="file-icon"></span>
                                                                <span class="dropify-filename-inner"></span>
                                                            </p>
                                                            <p class="dropify-infos-message">Drag and drop or click to replace</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                </div>
                            </div>
                            <br>

                            <button type="submit" class="btn btn-raised btn-primary btn-round  waves-effect " >Add Product</button>

                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
</section>
@endsection
