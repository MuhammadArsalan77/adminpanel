<?php

namespace App\Listeners;

use App\Events\newCustomerRegisterEvent;
use App\Models\User;
use App\Notifications\newCustomerRegisterNotification;
use App\Notifications\RegisterUserNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;

class newCustomerRegisterListener
{
     /**
     * Create the event listener.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     //
    // }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(newCustomerRegisterEvent $event)
    {
        $admin=User::where('id','1')->get();
        Notification::send($admin,new newCustomerRegisterNotification($event->user));
    }
}
