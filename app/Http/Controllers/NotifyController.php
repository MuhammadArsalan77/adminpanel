<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;

class NotifyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    //Show New Notifications
    public function newCustomerNotification(){
        $notifications=auth()->user()->unreadNotifications;

        //Show count of  unread notification on master page
        $unReadNotifications=count($notifications);
        $notifications->created_at->diffForHumans();
        

         View::share('title', $unReadNotifications);
        return view('LeftNavbar.new_customer_notification')->with([
            'notifications'=> $notifications
        ]);
    }
    //Mark Notifications
    public function markNotification(Request $request)
    {
        auth()->user()
            ->unreadNotifications
            ->when($request->input('id'), function ($query) use ($request) {
            return $query->where('id', $request->input('id'));
        })->markAsRead();

        return response()->noContent();
    }
}
