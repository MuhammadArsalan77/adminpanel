<?php

namespace App\Http\Controllers;

use App\Events\newCustomerRegisterEvent;
use App\Events\SendNewCustomerNotification;
use App\Listeners\SendNewUserNotification;
use App\Models\Banner;
use App\Models\Category;
use App\Models\CoupenCode;
use App\Models\Customer;
use App\Models\Logo;
use App\Models\Order_details;
use App\Models\pizza_details;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Hash;

class ApisCont extends Controller
{

    // Login Customer
   public function login(Request $request)
    {
        $user= Customer::where('email', $request->email)->first();

        if($user->active_status == 0){
            $user->active_status = true;
        }else{
            $user->active_status = false;
        }

            if (!$user || !Hash::check($request->password, $user->password)) {
                return response([
                    'Status'=>false,
                    'message' => ['These credentials do not match our records.']
                ], 404);
            }

             $token = $user->createToken('my-app-token')->plainTextToken;


             return response()->json(['status'=>true,'user'=>$user,'token'=>$token,'message'=>"successfully login"]);
    }

    // Register Customer
    public function register(Request $request)
    {
        $validatedData=$request->validate([
            'firstName'=>'required|max:55',
            'lastName'=>'required|max:55',
            'Contact'=>'required|min:11',
            'email'=>'required|email',
            'password'=>'required|min:6',
        ]);

        $validatedData['password'] = bcrypt($request->password);

        event(new newCustomerRegisterEvent($user = Customer::create($validatedData)));

        $user->active_status = true;

        $token = $user->createToken('my-app-token')->plainTextToken;
            if($user){
                return response()
                ->json(['success'=> true, 'user'=> $user, 'token'=> $token,
                        'message'=>"Registered Successfully",]);
            }
            else
            {
                return response()
                ->json(['success'=> false,
                'message'=>$user->errors()]);
            }

    }
    //Pizza Details
    public function ShowPizzaDetail(){
        $cus=pizza_details::all();
        if($cus)
        {
            return response()->json(["status"=>true,"data"=>$cus,'message'=>'Successfully Done']);
        }
        else
        {
            return response()->json(["status"=>false,'message'=>'No Record Found']);
        }
    }

    // Place Orders
    public function orders(Request $request){

        $re=new Order_details();
          $re->order_array = $request->order_array;
         // $re->pizza_id=$request->pizza_id;
          $re->Address=$request->Address;
          $re->special_note=$request->special_note;
          $re->customer_id=$request->customer_id;
          $re->order_type=$request->order_type;
          $re->lat=$request->lat;
          $re->long=$request->long;
          $re->total_amount=$request->total_amount;
          if($re->active_status == 0){
              $re->active_status = true;
          }else{
              $re->active_status = false;
          }
          $re->save();

          if($re)
          {
              return response()->json(["status"=>true,'message'=>'Order Done',"data"=>$re]);
          }
          else{
              return response()->json(["status"=>false,'message'=>'Sorry Order Not Done ']);
          }
      }
    public function CustomerOrderHistory($id)
    {

       $re= Customer::find($id);
       if($re)
        {
           $se= Order_details::orderBy('created_at','desc')->where('customer_id','=',$id)->get();
            if($se)
            {
                for($i = 0; $i<count($se); $i++)
                {

                    $se[$i]->order_array = json_decode($se[$i]->order_array);

                }
                return response()->json(['Status'=>true,'message'=>'Successfully shown Order History','data'=>$se]);
            }
            else
            {
                return response()->json(['Status'=>false,'message'=>' sorry No order Found']);
            }
        }
    }

    //Update Password
    public function changedPassword(Request $request){
        $this->validate($request, [
            'current' => 'required',
            'newpassword' => 'required|min:6',
            'password_confirmation' => 'required|same:newpassword'
        ]);

         $user = Customer::find($request->id);

        if (!Hash::check($request->current, $user->password)) {
            return response()->json(['errors' => ['current'=> ['Current password does not match']]],
        422);
        }

        $user->password = Hash::make($request->newpassword);
        $user->save();

         return $user;

    }

    //Forget Password
    // public function ForgetPassword(Request $request){
    //     // $this->validate($request, [
    //     //     'email' => 'required|min:6',
    //     //     'newpassword' => 'required|min:6',
    //     //     'password_confirmation' => 'required|same:newpassword'
    //     // ]);

    //     //  $user = Customer::find($request->email);
    //      $user=Customer::where(['email'=>$request->email])->get();

    //     if ($user) {

    //             $pass= Crypt::decrypt($user->password);
    //         return $pass;
    //             //return response()->json(['Status'=>true,'User'=>$user,'Message'=>'Password Successfully changed']) ;

    //     }
    //     // else{
    //     //     return response()->json(['errors' => ['current'=> ['Current email does not match']]],
    //     //     422);
    //     // }


    // }


    // Banners Api
    public function GetImage(){
        $re=Banner::all();
        if($re)
        {
            return response()->json(["Status"=>true,'message'=>'Images Shown Successfully','file'=>$re]);
        }
        else
        {
            return response()->json(["Status"=>false,'message'=>'Sorry No Record Found']);
        }
    }

    public function ShowCategories(){
        $cus=Category::all();
        if($cus)
        {
            return response()->json(["status"=>true,"data"=>$cus,'message'=>'Successfully Done']);
        }
        else
        {
            return response()->json(["status"=>false,'message'=>'No Record Found']);
        }
    }

    public function showCategory($id)
    {
        $re=Category::find($id);
        if($re)
        {
           $se= pizza_details::where('category_id','=',$id)->get();
            if($se)
            {
                return response()->json(['Status'=>true,'data'=>$se,'message'=>'All Record Show']);
            }

        }
        else
        {
            return response()->json(['Status'=>false,'message'=>'No Record Found']);
        }

    }
    public function coupenCodeData(Request $request){
        $re=CoupenCode::where('coupen_Codee', '=', $request->coupen_Codee)->get();
        //return $re;
        if($re)
        {
            return response()->json(['Status'=>true,'data'=>$re,'message'=>'Discount available']);
        }
        else
        {
            return response()->json(['Status'=>false,'message'=>'Sorry! No discount available']);
        }
    }
    public function GetLogo()
    {
        $re=Logo::all();
        if($re)
        {
            return response()->json(["Status"=>true,'message'=>'Images Shown Successfully','file'=>$re]);
        }
        else
        {
            return response()->json(["Status"=>false,'message'=>'Sorry No Record Found']);
        }
    }
}

