<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use App\Models\Category;
use App\Models\CoupenCode;
use App\Models\Customer;
use App\Models\Logo;
use App\Models\Order_details;
use App\Models\pizza_details;
use App\Notifications\RegisterUserNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PHPUnit\Framework\Constraint\Count;
use Symfony\Component\Finder\Iterator\CustomFilterIterator;

class AdminCont extends Controller
{

        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }


    //Show Customer Records
    public function ShowData(Request $request){
        $cus=Customer::all();
        if($cus)
        {
            return view('LeftNavbar.CustomerRec')->with('data',$cus);
        }
    }

    //Update Customer Status(Block And UnBlock)
    public function status(Request $request)
    {
       $us= Customer::find($request->id);
        $us->active_status=$request->active_status;
        $us->save();
        if($us)
        {
            return ["status"=>true];
        }
    }

    //Add Pizza Form
    public function Addpizza()
    {
        $re=Category::all();

        return view('LeftNavbar.AddPizzaDetial')->with('data',$re);
    }

    //Add Pizza In DB
    public function Addedpizza(Request $request)
    {
        if($request->hasFile('pizza_Image'))
        {

        $file=$request->file('pizza_Image');
        $extension=$file->getClientOriginalExtension();
        $filename=time().'.'.$extension;
        $file->move('uploads/pizzaImages/',$filename);


        $re= new pizza_details();
        $re->pizza_name=$request->pizza_name;
        $re->pizza_description=$request->pizza_description;
        $re->deal_code=$request->deal_code;
        // $re->pizza_Image=$request->pizza_Image;
        $re->pizza_price_medium=$request->pizza_price_medium;
        $re->pizza_price_small=$request->pizza_price_small;
        $re->pizza_price_large=$request->pizza_price_large;
        $re->pizza_price_xl=$request->pizza_price_xl;
        $re->category_id=$request->category_id;
        $re->pizza_Image=$filename;

        $re->save();

        return redirect()->back()->with('message','done');
        }
    }


    //Show Pizza Records
    public function ShowPizzaDetail()
    {
        $cus=pizza_details::orderByDesc('id')->orderBy('id')->get();
        if($cus)
        {
            return view('LeftNavbar.PizzaDetails')->with('data',$cus);
        }
    }

    //Delete Pizza Records
    public function DeleteRecord($id)
    {
        $re=pizza_details::find($id);
        $se=$re->pizza_Image;
        unlink("uploads/pizzaImages/".$se);
        $re=pizza_details::find($id)->delete();
        if($re)
        {
            return response()->json(['Success'=>'Record has been Deleted']);
        }
    }

    //Edit Pizza Record
    public function editPizza($id)
    {
        $re= pizza_details::find($id);
        return view('LeftNavbar.EditPizzaDetail')->with('data',$re);
    }


    //Update Pizza Details
    public function updateData(Request $request,$id)
    {
    $re= pizza_details::find($id);
    $re->pizza_name=$request->input('pizza_name');
    $re->pizza_description=$request->input('pizza_description');
    $re->deal_code=$request->input('deal_code');
    $re->pizza_price_medium=$request->input('pizza_price_medium');
    $re->pizza_price_small=$request->input('pizza_price_small');
    $re->pizza_price_large=$request->input('pizza_price_large');
    $re->pizza_price_xl=$request->input('pizza_price_xl');
    if($request->hasFile('pizza_Image')==$re->pizza_Images)
    {
        $re->pizza_Image;
    }elseif($request->hasFile('pizza_Image'))
    {


        $file=$request->file('pizza_Image');
        $extension=$file->getClientOriginalExtension();
        $filename=time().'.'.$extension;
        $file->move('uploads/pizzaImages/',$filename);
        $se=$re->pizza_Image;
        unlink("uploads/pizzaImages/".$se);
        $re->pizza_Image=$filename;
    }
    $re->save();
    return redirect('ShowPizzaDetails')->with('message','done');
    }

     //Show Pizza Orders to Admin
     public function orders(Request $request)
     {

         $data=Order_details::all();

         for($i = 0;$i<count($data); $i++){
             $cust = Customer::find($data[$i]->customer_id);
             $data[$i]->customer_name = $cust->firstName." ". $cust->lastName;
             $data[$i]->customer_contact = $cust->Contact;
             $data[$i]->customer_email = $cust->email;
             $tdata = json_decode($data[$i]->order_array);

             $data[$i]->tdata = $tdata;

         }
         return view('LeftNavbar.OrderDetail')->with('data',$data);
     }


     //Show Delivered Ordered
     public function DeliveredOrder(Request $request)
     {
          $data=Order_details::all();

         for($i = 0;$i<count($data); $i++){
             $cust = Customer::find($data[$i]->customer_id);
             $data[$i]->customer_name = $cust->firstName." ". $cust->lastName;
             $data[$i]->customer_contact = $cust->Contact;
             $data[$i]->customer_email = $cust->email;
             $tdata = json_decode($data[$i]->order_array);

             $data[$i]->tdata = $tdata;

         }
             return view('LeftNavbar.DeliveredOrders')->with('data',$data);

     }


    //Order Status (Pending or Delivered)
    public function statuss(Request $request)
    {
        $us= Order_details::find($request->id);
         $us->active_status=$request->active_status;
         $us->save();
         if($us)
         {
             return ["status"=>true];
         }
    }

    //Add Banners
    Public function Banners()
    {
        return view('LeftNavbar.Banner');
    }

    //Show Banners to Admin
    Public function AllBanners()
    {
        $re=Banner::all();
        if($re)
        {
            return view('LeftNavbar.ShowBanner')->with('data',$re);
        }
    }

    //Delete Banners
    public function DeleteRecs($id)
    {
        $re=Banner::find($id);
        $se=$re->Images;
        unlink("uploads/pizzaImages/".$se);
        $re=Banner::find($id)->delete();
        if($re)
        {
            return response()->json(['Success'=>'Record has been Deleted']);
        }
    }

    //Add Banner Images
    public function Images(Request $request)
    {
        $re= new Banner();
        if($request->hasFile('Images'))
        {
        $file=$request->file('Images');
        $extension=$file->getClientOriginalExtension();
        $filename=time().'.'.$extension;
        $file->move('uploads/pizzaImages/',$filename);
        $re->Images=$filename;
        $re->save();

        }
        return redirect('Banner')->with('message','done');
    }


    public function AddCategory()
    {
        return view('LeftNavbar.AddCategory');
    }

    public function AddedCategory(Request $request)
    {
        if($request->hasFile('catgory_icon'))
        {
        $file=$request->file('catgory_icon');
        $extension=$file->getClientOriginalExtension();
        $filename=time().'.'.$extension;
        $file->move('uploads/pizzaImages/',$filename);

        $re= new Category();
        $re->catory_name=$request->catory_name;
        $re->catgory_icon=$filename;
        $re->save();
        return redirect()->back()->with('message','done');
        }
    }

    public function ShowCategory()
    {
        $re=Category::all();
        if($re)
        {
            return view('LeftNavbar.ShowCategory')->with('data',$re);
        }
    }



    //Delete Category
    public function DeleteCategory($id)
    {
        $re=Category::find($id);
        $se=$re->catgory_icon;
        unlink("uploads/pizzaImages/".$se);
        $re=Category::find($id)->delete();

        //unlink("uploads/pizzaImages/".$re->catgory_icon);
        if($re)
        {
            return response()->json(['Success'=>'Record has been Deleted']);
        }
    }

    public function EditCategory($id)
    {
       $re= Category::find($id);
       if($re)
       {
        return view('LeftNavbar.EditCategory')->with('data',$re);
       }

    }
    public function UpdateCatgory(Request $request,$id)
    {
        $re=Category::find($id);
        if($re)
        {

            $re->catory_name=$request->input('catory_name');


                if($request->hasFile('catgory_icon'))
                {
                $file=$request->file('catgory_icon');
                $extension=$file->getClientOriginalExtension();
                $filename=time().'.'.$extension;
                $file->move('uploads/pizzaImages/',$filename);
                $se=$re->catgory_icon;
                unlink("uploads/pizzaImages/".$se);
                $re->catgory_icon=$filename;
                }
            $re->save();
            return redirect('ShowCategory')->with('message','done');
        }

    }
    public function addCoupen()
    {
        return view('LeftNavbar.AddCoupen');
    }
    public function addedCoupen(Request $request)
    {
        // $validated = $request->validate([
        //     'DiscountPercent' => 'required|min:1|max:100',

        // ]);
       $re= CoupenCode::Create($request->all());
       if($re)
       {
        return redirect()->back()->with('message','done');
       }

    }

    public function CoupenData(){
        $re=CoupenCode::all();
        if($re)
        {
            return view('LeftNavbar.CoupenData')->with('data',$re);
        }
    }
    public function EditCopen($id)
    {
        $re= CoupenCode::find($id);
        if($re)
        {
         return view('LeftNavbar.EditCopen')->with('data',$re);
        }
    }
    public function UpdatedCoupen(Request $request,$id){
        $re=CoupenCode::find($id);
        if($re)
        {
            $re->coupen_Codee=$request->input('coupen_Codee');
            $re->DiscountPercent=$request->input('DiscountPercent');
            $re->save();
            return redirect('CoupenData')->with('message','done');
        }

    }
    public function DeleteCoupen($id)
    {
        $re=CoupenCode::find($id)->delete();
        if($re)
        {
            return response()->json(['Success'=>'Record has been Deleted']);
        }
    }
    public function LogoImage(Request $request)
    {
        $logo=Logo::all();
        if($logo)
        {
            return view('LeftNavbar.showlogo')->with('logo',$logo);
        }

    }
    public function EditLogo($id)
    {
       $re= Logo::find($id);
       if($re)
       {
        return view('LeftNavbar.EditLogo')->with('data',$re);
       }

    }
    public function UpdatedLogo(Request $request,$id)
    {
        $re=Logo::find($id);
        if($re)
        {
            if($request->hasFile('Images'))
            {
                $file=$request->file('Images');
                $extension=$file->getClientOriginalExtension();
                $filename=time().'.'.$extension;
                $file->move('uploads/pizzaImages/',$filename);
                $se=$re->Images;
                unlink("uploads/pizzaImages/".$se);
                $re->Images=$filename;
            }
            $re->save();
            return redirect('Logo')->with('logo','done');
        }

    }
}
