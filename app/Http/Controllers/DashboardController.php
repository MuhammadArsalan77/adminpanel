<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Customer;
use App\Models\Order_details;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }


    //Admin Dashboard
    public function dashboard(){
        $cus=Customer::all();
        $totalCustomer=count($cus);
        $or=Order_details::all();
        $totalOrder=count($or);
        $category=Category::all();
        $totalCategory=count($category);
        $notifications=auth()->user()->unreadNotifications;

        $customers=Customer::select(DB::raw("COUNT(*) as count"))
        ->whereYear('created_at',date('Y'))
        ->groupBy(DB::raw("Month(created_at)"))
        ->pluck('count');
        $months=Customer::select(DB::raw("Month(created_at) as month"))
        ->whereYear('created_at',date('Y'))
        ->groupBy(DB::raw("Month(created_at)"))
        ->pluck('month');
        $datas= array(0,0,0,0,0,0,0,0,0,0,0,0);
        foreach($months as $index => $month)
        {
            $datas[$month]=$customers[$index];
        }


        return view('LeftNavbar.Dashboard')->with([
            "order" => $totalOrder,
            "customer" => $totalCustomer,
            "category" => $totalCategory,
            "notifications" => $notifications,
            "datas" => $datas
        ]);

    }

    
}
