<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class pizza_details extends Model
{
    use HasFactory;
    protected $fillable = [
        'pizza_name',
        'pizza_description',
        'deal_code',
        'pizza_Image',
        'pizza_price_medium',
        'pizza_price_small',
        'pizza_price_large',
        'pizza_price_xl',
        'category_id',
    ];
}
