<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order_details extends Model
{
    use HasFactory;
    protected $fillable = [
        'pizza_quantity',
        'pizza_type',
        'pizza_price',
        'Address',
        'special_note',
        'active_status',
        'customer_id',
        'lat',
        'long',
        'total_amount',
        'order_array',
        'order_type',
    ];

}
