<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class Customer extends Model
{
    use HasApiTokens,HasFactory,Notifiable;
    protected $fillable = [
        'firstName',
        'lastName',
        'email',
        'Contact',
        'password',
        'ActiveStatus',
    ];

    // protected $hidden = [
    //     'password',
    //     'remember_token',
    // ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
   
}
