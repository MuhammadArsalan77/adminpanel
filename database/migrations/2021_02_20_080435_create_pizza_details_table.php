<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePizzaDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pizza_details', function (Blueprint $table) {
            $table->id();
            $table->string('pizza_name');
            $table->String('pizza_description')->nullable();
            $table->string('deal_code');
            $table->string('pizza_Image');
            $table->string('pizza_price_medium')->nullable();
            $table->string('pizza_price_small')->nullable();
            $table->string('pizza_price_large')->nullable();
            $table->string('pizza_price_xl')->nullable();
            $table->bigInteger('category_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pizza_details');
    }
}
